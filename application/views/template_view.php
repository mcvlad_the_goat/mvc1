<!Doctype html>
<html>
<head>
	<meta charset="utf-8">
    <title>MVC в деле!</title>

</head>
<body><link href="application/css/mainstyle.css" rel="stylesheet">
	<nav><ol><a href="main">О компании</a></ol><ol><a href="contacts">Контакты</a></ol><ol><a href="guestbook">Гостевая книга</a></ol>
	<?php if(!isset($_SESSION["auth"])) include_once "auth_form.html";
	else include_once("admin_form.html"); ?> 
	</nav>
	<hr noshade>
	<?php include 'application/views/'.$content_view; ?>
</body>
</html>